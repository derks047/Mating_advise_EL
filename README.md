Pipeline to check carriers of lethal haplotypes in the population. Results are written to [breed]_results.tsv

The script reads a plink file (e.g. VVVV_plink.bed VVVV_plink.bim VVVV_plink.fam) and phases the samples against an exisiting reference population. The final column in the results file lists all the carriers of the lethal allele

Example run:  ./pipeline_breeding_advise_EL.py -p VVVV_plink -b VVVV

test
