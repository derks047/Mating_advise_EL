#!/usr/bin/env python
## Martijn Derks

try:
	import argparse
	from collections import OrderedDict
	import subprocess
	import gzip
	import os
except ImportError:
    sys.exit("""You need the following packages: argparse, collections, subprocess, gzip, os! run "pip install <package> --user" to install missing packages.""")

parser = argparse.ArgumentParser( description='Analyse haplotypes to identify carriers of previously identified lethal recessives using phased population data, output written to [Breed]_results.tsv')
parser.add_argument("-p", "--plink_file", help="Plink file", nargs=1)
parser.add_argument("-b", "--breed", help="Breed (e.g. ZZZZ)", nargs=1)

class HF:

	def __init__(self):
		self.outfile=open(breed+"_results.tsv","w")
		self.lethal_carriers_dic={}

	def convert_plink_to_vcf(self, plinkfile):
		os.system("plink2/plink --bfile "+plinkfile+" --recode vcf-iid --out "+plinkfile)
		self.plinkfile = plinkfile
	
	def phase_vcf(self, plinkfile,chr, start, stop, lethal_id):
		os.system("java -Xmx8g -jar beagle/beagle.03Jul18.40b.jar gt="+plinkfile+".vcf chrom="+chr+":"+str(int(start)-5000000)+"-"+str(int(stop)+5000000)+" out="+plinkfile+"."+lethal_id+".phased ne=100 nthreads=4")
		
	def get_vcf_samples(self,vcffile):
		""" Get all the phased samples and write in sample dictionary """
		cmd = "zcat "+vcffile+" | grep -i '#CHROM'"
		samples = subprocess.check_output(cmd, shell=True)
		self.sample_dic = {}
		scount = 0
		self.sample_list = samples.strip().split("\t")[9:]
		for sample in self.sample_list:
			if sample not in self.lethal_carriers_dic:
				self.lethal_carriers_dic[sample] = []
			self.sample_dic[scount] = sample
			self.sample_dic[scount+1] = sample
			scount += 2

	def sliding_windows(self, breed, genotype_dic, chr, start,stop,lethal_id):
		""" Scan the haplotypes per sliding window """
		self.outfile=open(breed+"_results.tsv","a")
		header = ["Chr", "Start","Stop","Haplotype","Genotype","Markers","Haplotype_length","#Haplotypes_in_window","Frequencies_haplotypes_in_window","Haplotype_count","#Homozygous","Haplotype_frequency","Homozygotes_expected","Heterozygote_progeny"]
		self.outfile.write("\t".join(header)+"\n")
		#int(self.length)
		
		self.marker_dic = {}
		self.window_key = str(chr)+"_"+str(start)+"_"+str(stop)
		hap_dic={}
		start,stop=int(start),int(stop)
		for marker in sorted(genotype_dic.keys()):
			if int(marker) >= start and int(marker) <= stop:
				snp=genotype_dic[marker]
				count=0
				self.marker_dic[marker] = [snp[1],snp[3],snp[4]]
				for gt in snp[9:]:
					gt=gt.split("|")
					if count in hap_dic:
							hap_dic[count].append(gt[0])
							hap_dic[count+1].append(gt[1][:1])
					else:
							hap_dic[count]=[gt[0]]
							hap_dic[count+1]=[gt[1][:1]]
					count+=2
		hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic = self.get_frequencies(hap_dic, start, stop)
		self.window_statistics(hap_freq_dic)
		lethal_dic=self.summarize_hap_analysis_and_report(hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic,chr,start,stop,lethal_id)
		return lethal_dic

	def read_haps_vcffile(self, vcffile):
		""" Parse the phased file from beagle in a genotype dictionary """
		genotype_dic={}
		with gzip.open(vcffile,'r') as hapsfile:
			for snp in hapsfile:
				snp=snp.strip()
				if not snp.startswith("#"):
					snp=snp.strip().split()
					snp_pos = int(snp[1])
					genotype_dic[snp_pos] = snp
		hapsfile.close()
		return genotype_dic

	def get_frequencies(self, hap_dic, start, stop):
		""" Count haplotype frequencies and check for homozygosity """
		hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic={},{},{},{}
		count,self.thap =0,0
		for hap in hap_dic:
			haplotype="".join(hap_dic[hap])
			if haplotype in sample_dic: ## Get all the samples with this haplotype
				sample_dic[haplotype].append(self.sample_dic[hap])
			else:
				sample_dic[haplotype] = [self.sample_dic[hap]]

			if haplotype in hap_freq_dic: ## Count haplotype occurrence
				hap_freq_dic[haplotype] += 1
			else:
				hap_freq_dic[haplotype] = 1
			self.thap += 1
			if count == 1: ## Check if haplotype occurs in homozygous state
				if haplotype == "".join(hap_dic[hap-1]):
					if haplotype in hap_homozygosity_dic:
						hap_homozygosity_dic[haplotype] += 1
					else:
						hap_homozygosity_dic[haplotype] = 1
					if haplotype in zygosity_dic:
							zygosity_dic[haplotype][0].append(self.sample_dic[hap])
					else:
							zygosity_dic[haplotype] = [[self.sample_dic[hap]],[]]
				else:
					if haplotype in zygosity_dic:
							zygosity_dic[haplotype][1].append(self.sample_dic[hap])
					else:
						zygosity_dic[haplotype] = [[],[self.sample_dic[hap]]]
					shaplotype = "".join(hap_dic[hap-1])
					if shaplotype in zygosity_dic:
							zygosity_dic[shaplotype][1].append(self.sample_dic[hap-1])
					else:
							zygosity_dic[shaplotype] = [[],[self.sample_dic[hap-1]]]
				count=0
			else:
				count +=1
		return hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic

	def window_statistics(self, hap_freq_dic):
		""" Get window statistics from haplotypes """
		self.window_haplotype_frequencies={}
		hap_freq_dic = OrderedDict(sorted(hap_freq_dic.items(), key=lambda x: x[1]))
		for hap in hap_freq_dic:
			tcount = hap_freq_dic[hap]
			cfreq = float(tcount)/self.thap
			if cfreq > 0.005:
					self.window_haplotype_frequencies[hap] = round(cfreq,3)
		
	def summarize_hap_analysis_and_report(self, hap_freq_dic,sample_dic,zygosity_dic,hap_homozygosity_dic,chr,start,stop,lethal_id): 
		""" Report output """
		tcount=0
		hap_freq_dic = OrderedDict(sorted(hap_freq_dic.items(), key=lambda x: x[1]))	
		for hap in hap_freq_dic:
			tcount = hap_freq_dic[hap]
			afreq = float(tcount)/self.thap
			tanimals = self.thap/2
			homozygous_expected = tanimals * ((afreq)**2) ## expected homozygotes based on frequency

			observed_homozygotes = len(zygosity_dic[hap][0])
			if afreq>0.005 and homozygous_expected > 10 and observed_homozygotes <7: # and observed_homozygotes <= 5: ## Get missing homozygotes
				carriers = sample_dic[hap]

				if not hap in hap_homozygosity_dic:
					nhomozygous = 0
				else:
					nhomozygous = hap_homozygosity_dic[hap]

				markers, genotypes = self.get_haplotype_SNPs(hap)
				outlist = [str(chr), str(start), str(stop), str(hap), genotypes, markers, str(len(hap)), str(len(self.window_haplotype_frequencies)), ",".join(map(str, sorted(self.window_haplotype_frequencies.values(), reverse=True))), str(tcount), str(nhomozygous), str("%.3f" % afreq), str("%.2f" % homozygous_expected), ",".join(carriers)]
				self.outfile.write("\t".join(outlist)+"\n\n")
				
				for sample in self.sample_list:
					if sample in carriers:
						self.lethal_carriers_dic[sample].append(lethal_id)
						
	def get_haplotype_SNPs(self, hap):
		""" Get the markers that make up the haplotype """
		markers,genotypes = [],[]
		i = 0
		for gt in hap:
			marker = sorted(self.marker_dic.keys())[i]
			alleles = "".join(self.marker_dic[marker][1:])
			if int(gt) == 0:
					genotypes.append(self.marker_dic[marker][1])
			else:
					genotypes.append(self.marker_dic[marker][2])
			i+=1
			markers.append(self.marker_dic[marker][0]+"_"+alleles)
	
		return ",".join(markers), "".join(genotypes)
		
	def write_output(self):
		for sample in self.lethal_carriers_dic:
			self.outfile.write(sample+"\t"+",".join(self.lethal_carriers_dic[sample])+"\n")
		self.outfile.close()
		os.system("rm "+self.plinkfile+".vcf")
	
args = parser.parse_args()
plink = args.plink_file[0]
breed = args.breed[0]

H=HF()
H.convert_plink_to_vcf(plink)
if breed.startswith("Z"):
	lethal_id,chr,start,stop="LW19","18","41000000","41500000"
	print chr,start,stop
	H.phase_vcf(plink,chr,start,stop,lethal_id)
	phased_vcffile = plink+"."+lethal_id+".phased.vcf.gz"
	H.get_vcf_samples(phased_vcffile)
	genotype_dic=H.read_haps_vcffile(phased_vcffile)
	H.sliding_windows(breed,genotype_dic, chr,start,stop,lethal_id)
	H.write_output()
if breed.startswith("L"):
	lethals = [["LA1","3","42500000","47500000"],["LA2","13","197000000","197500000"],["LA3","6","52500000","54000000"]]
	for EL in lethals:
		lethal_id,chr,start,stop = EL
		print chr,start,stop
		H.phase_vcf(plink,chr,start,stop,lethal_id)
		phased_vcffile = plink+"."+lethal_id+".phased.vcf.gz"
		H.get_vcf_samples(phased_vcffile)
		genotype_dic=H.read_haps_vcffile(phased_vcffile)
		H.sliding_windows(breed,genotype_dic,chr,start,stop,lethal_id)
	H.write_output()
if breed.startswith("V"):
	lethal_id,chr,start,stop="DU1","12","38500000","39000000"
	print chr,start,stop
	H.phase_vcf(plink,chr,start,stop,lethal_id)
	phased_vcffile = plink+"."+lethal_id+".phased.vcf.gz"
	H.get_vcf_samples(phased_vcffile)
	genotype_dic=H.read_haps_vcffile(phased_vcffile)
	H.sliding_windows(breed,genotype_dic, chr,start,stop,lethal_id)	
	H.write_output()
if breed.startswith("E"):
	lethal_id,chr,start,stop="SY1","6","47250000","48750000"
	print chr,start,stop
	H.phase_vcf(plink,chr,start,stop,lethal_id)
	phased_vcffile = plink+"."+lethal_id+".phased.vcf.gz"
	H.get_vcf_samples(phased_vcffile)
	genotype_dic=H.read_haps_vcffile(phased_vcffile)
	H.sliding_windows(breed,genotype_dic, chr,start,stop,lethal_id)	
	H.write_output()